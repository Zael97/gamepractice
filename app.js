const express = require('express');
const socketio = require('socket.io');

const port = 9000;


const app = express();

app.use(express.static('public'));

app.set('view engine', 'pug');

const server = app.listen(port, () => {
    `Server listen at port ${port}.Press Ctrl+C to stop. `;
})

const player = app.route('/player');

player.get((req, res) => {
    res.render('player');
});


const socketServer = socketio(server);


const playerList = [];

socketServer.on('connection', socket => {
    const player = socket.handshake.query.username;
    socket.emit('playerAvailables', playerList);
    socket.on('move', newPosition => {
        socketServer.emit('updateBoard', newPosition);
    });
    socketServer.emit('playerJoined', player);
    playerList.push(player);
});