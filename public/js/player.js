const username = prompt('Nombre de jugador?');

const socket = io('http://localhost:9000', { query: { username: username } });

socket.on('connect', () => {
    if (socket.connected) {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Conexión exitosa!',
            showConfirmButton: false,
            timer: 2000
        });
        document.querySelector('#username-status-container p').innerText = username;
        document.querySelector('#username-status').style.background = '#00e676';
    } else {
        Swal.fire({
            position: 'center',
            icon: 'wrong',
            title: 'Conexión fallida',
            showConformationButton: false,
            timer: 2000
        });
    }
})
socket.on('playerJoined', player => {
    createPlayerAvatar(player);
});
socket.on('playerAvailables', playerList => {
    playerList.forEach(player => {
        createPlayerAvatar(player);
    });
});

socket.on('updateBoard', newPosition => {
    console.log(newPosition);
    let ficha = document.querySelector(`#avatar-${newPosition.player}`);
    ficha.style.top = `${newPosition.position.y}px`;
    ficha.style.left = `${newPosition.position.x}px`;
});



function moveBall(direction) {
    let ficha = document.querySelector(`#avatar-${username}`);
    let x = ficha.getBoundingClientRect().x;
    let y = ficha.getBoundingClientRect().y;
    switch (direction) {
        case 'abajo':
            y += 10;
            break;
        case 'arriba':
            y -= 10;
            break;
        case 'izquierda':
            x -= 10;
            break;
        case 'derecha':
            x += 10;
            break;
    }
    ficha.style.top = `${y}px`;
    ficha.style.left = `${x}px`;
    socket.emit('move', { player: username, position: { x: x, y: y } });
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function createPlayerAvatar(playerName) {
    const tablero = document.querySelector('#player-container-board');
    const playerChar = document.createElement('DIV');
    playerChar.style.cssText = `width: 20px; height:20px; border-radius: 50%; background:red; position: absolute;`;
    playerChar.setAttribute('id', `avatar-${playerName}`);
    playerChar.title = playerName;
    tablero.appendChild(playerChar);
}